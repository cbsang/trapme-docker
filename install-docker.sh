#
# This script is used to install docker on an Ubuntu machine.
# The version could be Ubuntu 16.04 or 18.04
#
echo "Prepare fetching docker software"
sudo apt-get -y update
sudo apt-get -y install curl
# Key to authorize software from docker's own repository
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# Add docker repository to apt
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
echo "Get and install latest docker CE software"
sudo apt-get -y update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
echo 'Create group "docker" and add yourself. Avoids having to "sudo docker ..."'
sudo groupadd -f docker
sudo usermod -aG docker $USER
